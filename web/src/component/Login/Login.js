import './Login.css';
import React from 'react';
import { Route, Routes, Link } from "react-router-dom";
import Sidebar from "../../component/Sidebar/Sidebar";
import Google from "../../image/icons8-google.svg";
import Trangchu from "./../pages/Trangchu";
import Register from "./register"; 

function dangnhap() {
  return (
    <main className='body_login'>
      <div className=' flex justify-center items-center h-screen'>
        <div className="w-full max-w-xs">
          <form className="bg-font shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <h1 className='text-center text-2xl pb-5'> Đăng nhập tài khoản </h1>
            <div className="mb-4">
              <label
                className="text-black text-sm font-bold mb-2"
                htmlFor="username"
              >
                Tài khoản
              </label>
              <input
                className="shadow border rounded w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
                placeholder="Tài khoản"
              />
            </div>
            <div className="mb-6">
              <label
                className="text-black text-sm font-bold mb-2"
                htmlFor="password"
              >
                Mật khẩu
              </label>
              <input
                className="shadow border rounded w-full py-2 px-3 text-black mb-3 leading-tight focus:outline-none focus:shadow-outline"
                id="password"
                type="password"
                placeholder="Mật khẩu"
              />
            </div>
            <div className="flex items-center justify-between">
              <Link to="/Home/Trangchu">
                <button
                  className="button_login text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                  type="button"
                >
                  Đăng nhập
                </button>
              </Link>

              <a
                className="inline-block align-baseline font-bold text-sm text"
                href="#"
              >
                Quên mật khẩu ?
              </a>
              <Routes>
                <Route path="#" element={<Sidebar />} />
              </Routes>

            </div>
            <h2 className=" text-center p-5"> Hoặc đăng nhập bằng 
              <Link to="/DangKy">
                <a href={<Register />} className='underline font-bold text-sm text' >Đăng ký</a>
              </Link>
            </h2> 
            
            <div className='flex items-center justify-between '>
                <button
                type="button"
                class="mb-2 flex rounded px-6 py-2.5 text-xs font-medium uppercase leading-normal text-white"
                style={{backgroundColor: '#1da1f2'}}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-4 w-4"
                  fill="currentColor"
                  viewBox="0 0 24 24">
                  <path
                    d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z" />
                </svg>
                facebook
              </button>

              <button
                type="button"
                class="mb-2 flex rounded px-6 py-2.5 text-xs font-medium uppercase leading-normal text-black border border-black "
                style={{backgroundColor: 'white'}}>
                <img src={Google} alt='google logo' />
                google
              </button>
            </div>
            <Routes>
              <Route path="/Home" element={<Sidebar />} >
                 <Route path="Trangchu" element={<Trangchu />} />
              </Route>
             
              <Route path='/DangKy' element={<Register />} />
            </Routes>
          </form>     
        </div>
      </div>
    </main>
  );
}

export default dangnhap;
