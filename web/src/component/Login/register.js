import React from 'react';
import "./register.css";
import {Link } from "react-router-dom";

export default function register() {
  return (
    <main className='body_regiter'>
      <div className='flex justify-center items-center h-screen'>
        <div className="w-full max-w-xs">
          <form className="bg-font shadow-md rounded px-8 pt-6 pb-8 mb-4 w-96">
            <h1 className='text-center text-2xl pb-5'> Đăng ký tài khoản </h1>
            <div className="mb-4">
              <label
                className="text-black text-sm font-bold mb-2"
                htmlFor="Ten"
              >
                Tên 
              </label>
              <input
                className="shadow border rounded w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
                id="Ten"
                type="text"
                placeholder="Tên"
              />
            </div>
            <div className="mb-4">
              <label
                className="text-black text-sm font-bold mb-2"
                htmlFor="HoTen"
              >
                Họ tên
              </label>
              <input
                className="shadow border rounded w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
                id="HoTen"
                type="text"
                placeholder="Họ tên"
              />
            </div>
            <div className="mb-4">
              <label
                className="text-black text-sm font-bold mb-2"
                htmlFor="Email"
              >
                Email
              </label>
              <input
                className="shadow border rounded w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
                id="Email"
                type="email"
                placeholder="Email"
              />
            </div>
            <div className="mb-4">
              <label
                className="text-black text-sm font-bold mb-2"
                htmlFor="username"
              >
                Tài khoản
              </label>
              <input
                className="shadow border rounded w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
                placeholder="Tài khoản"
              />
            </div>
            <div className="mb-6">
              <label
                className="text-black text-sm font-bold mb-2"
                htmlFor="password"
              >
                Mật khẩu
              </label>
              <input
                className="shadow border rounded w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
                id="password"
                type="password"
                placeholder="Mật khẩu"
              />
            </div>
            <div className="flex items-center justify-between">
              <Link to="/Login">
              <button
                className="button_reginster text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="button"
              >
                Đăng ký
              </button>
              </Link>
            </div>

          </form>     
        </div>
      </div>
    </main>
  )
}
