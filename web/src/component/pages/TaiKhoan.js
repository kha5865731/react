import React from 'react';
import "./TaiKhoan.css";
import Person from "./../../image/person.jpg";

export default function About() {
  return (
    <main>
      <div className='Box_TaiKhoan '>
        <div>
          <img className='avatar_TaiKhoan' src={Person} alt='user'></img>
          <button className='Button_TK'>
            Đổi ảnh avatar
          </button>
        </div>
        <form>
          <h1 style={{fontFamily:"Arial, Helvetica, sans-serif",fontSize:"22px"}}> 
            THÔNG TIN  CÁ NHÂN
          </h1>
          <div className='HoTen'>
            <div>
              <div>Tên đầu</div>
              <input type='text' className='input_TK' placeholder='Tên đầu'></input>
            </div>
            <div style={{marginLeft:"200px"}}>
              <div>Họ tên</div>
              <input type='text' className='input_TK' placeholder='Họ tên'></input>
            </div>
          </div>
          <div className='changeEmail'>
            <div >
              <div>Email</div>
              <input type='email' className='input_TK' placeholder='Email'></input>
            </div>
            <button className='Button_TK'>
              Thay đổi email
            </button>
          </div>
        
          <div style={{marginTop:"40px"}}>
            <div>Mật khẩu</div>
            <input type='password' className='input_TK' placeholder='Mật khẩu'></input>
          </div>

          <div className='DoiMatKhau'>
            <div >
              <div>Nhập mật khẩu cũ</div>
              <input type='password' className='input_TK' placeholder='Nhập mật khẩu cũ'></input>
            </div>
            <div style={{marginLeft:"50px"}}>
              <div>Nhập mật khẩu mới</div>
              <input type='password' className='input_TK' placeholder='Nhập mật khẩu mới'></input>
            </div>
            <div style={{marginLeft:"50px"}}>
              <div>Nhập lại mật khẩu mới</div>
              <input type='password' className='input_TK' placeholder='Nhập lại mật khẩu mới'></input>
            </div>
          </div>
          <div style={{marginTop:"15px"}}>
            <button className='Button_TK' style={{marginLeft:"0"}}>
              Lưu Tài khoản
            </button>
            <button className='Button_TK' style={{marginLeft:"150px"}} >
              Thay đổi tài khoản
            </button>
          </div>
        </form>
      </div>  
    </main>
  )
}

// https://dribbble.com/shots/3283631-Engine-Dashboard/attachments/707230?mode=media
