import React from 'react';
import CanvasJSReact from '@canvasjs/react-charts';
//var CanvasJSReact = require('@canvasjs/react-charts');
 
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default function App (){	
    const options = {
        animationEnabled: true,
        // exportEnabled: true,
        theme: "dark1", // "light1", "dark1", "dark2"
        title:{
            text: "Tỷ lệ mua đồ uống"
        },
        
        data: [{
            type: "pie",
            indexLabel: "{label}: {y}%",		
            startAngle: -90,
            dataPoints: [
                { y: 20, label: "Coca cola" },
                { y: 32, label: "Pesi" },
                { y: 20, label: "Chai Nước" },
                { y: 14, label: "Nước cam" },
                { y: 12, label: "Trà bí đao" },
                { y: 10, label: "Trà sữa" },
                { y: 20, label: "Trà sữa" },
                { y: 11, label: "Nước dừa" },
                { y: 20, label: "Nước táo" },
                { y: 14, label: "Nước táo" }
            ]
        }]
    }
    return(
      <main>
         <CanvasJSChart options = {options} 
				containerProps={{ width: 'auto', height: '300px' }}
                
			/>
			
      </main>
    )
}