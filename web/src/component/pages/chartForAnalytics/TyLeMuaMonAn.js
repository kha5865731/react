import React from 'react';
import CanvasJSReact from '@canvasjs/react-charts';
//var CanvasJSReact = require('@canvasjs/react-charts');
 
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default function App (){	
    const options = {
        animationEnabled: true,
        // exportEnabled: true,
        theme: "dark1", // "light1", "dark1", "dark2"
        title:{
            text: "Tỷ lệ mua món ăn"
        },
        
        data: [{
            type: "pie",
            indexLabel: "{label}: {y}%",		
            startAngle: -90,
            dataPoints: [
                { y: 20, label: "Phở gà" },
                { y: 32, label: "Hambeger" },
                { y: 20, label: "Burito" },
                { y: 14, label: "Cơm gà" },
                { y: 12, label: "Khoai tây chiên" },
                { y: 10, label: "Bánh mì" },
                { y: 20, label: "Sushi" },
                { y: 11, label: "Bánh sườn bò" },
                { y: 20, label: "Mì cay" },
                { y: 14, label: "Bánh cuốn" }
            ]
        }]
    }
    return(
      <main>
         <CanvasJSChart options = {options} 
				containerProps={{ width: 'auto', height: '300px'}}
                
			/>
			
      </main>
    )
}