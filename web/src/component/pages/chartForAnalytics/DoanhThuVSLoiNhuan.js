import React from 'react';
import CanvasJSReact from '@canvasjs/react-charts';
//var CanvasJSReact = require('@canvasjs/react-charts');
 
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default function App (){	
    const options = {
        title: {
            text: "Doanh thu và lợi nhuận"
        },
        theme: "dark1",
        toolTip: {
            shared: true
        },
        legend: {
            verticalAlign: "top"
        },
        axisY: {
            suffix: "%"
        },
        data: [{
            type: "stackedBar100",
            color: "#9bbb59",
            name: "Lợi nhuận",
            showInLegend: true,
            indexLabel: "{y}",
            indexLabelFontColor: "white",
            yValueFormatString: "#,###'%'",
            dataPoints: [
                { y: 20, label: "Phở gà" },
                { y: 32, label: "Hambeger" },
                { y: 20, label: "Burito" },
                { y: 14, label: "Cơm gà" },
                { y: 12, label: "Khoai tây chiên" },
                { y: 10, label: "Bánh mì" },
                { y: 20, label: "Sushi" },
                { y: 11, label: "Bánh sườn bò" },
                { y: 20, label: "Mì cay" },
                { y: 14, label: "Bánh cuốn" }
            ]
        },{
            type: "stackedBar100",
            color: "#7f7f7f",
            name: "Doanh thu",
            showInLegend: true,
            indexLabel: "{y}%",
            indexLabelFontColor: "white",
            yValueFormatString: "#,###'%'",
            dataPoints: [
                { label: "Phở gà",   y: 15 },
                { label: "Hambeger",   y: 21 },
                { label: "Burito",   y: 23 },
                { label: "Cơm gà",   y: 32 },
                { label: "Khoai tây chiên",   y: 37 },
                { label: "Bánh mì",   y: 39 },
                { label: "Sushi",   y: 41 },
                { label: "Bánh sườn bò",   y: 51 },
                { label: "Mì cay",   y: 51 },
                { label: "Bánh cuốn",   y: 82 }
            ]
        }]
    }
    return(
      <main>
         <CanvasJSChart options = {options} 
				containerProps={{ width: 'auto', height: '270px'}}
                
			/>
			
      </main>
    )
}