import React from 'react';
import CanvasJSReact from '@canvasjs/react-charts';
//var CanvasJSReact = require('@canvasjs/react-charts');
 
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default function App (){	
  const options = {
    animationEnabled: true,
    title:{
      text: "Lợi nhuận mua đồ uống"
    },
    theme: "dark1",
    axisX: {
      valueFormatString: "MMM"
    },
    axisY: {
      title: "Sales (in USD)",
      prefix: "$"
    },
    data: [{
      yValueFormatString: "$#,###",
      xValueFormatString: "MMMM",
      type: "spline",
      dataPoints: [
        { y: 20, label: "Coca cola" },
        { y: 32, label: "Pesi" },
        { y: 20, label: "Chai Nước" },
        { y: 14, label: "Nước cam" },
        { y: 12, label: "Trà bí đao" },
        { y: 10, label: "Trà sữa" },
        { y: 20, label: "Trà sữa" },
        { y: 11, label: "Nước dừa" },
        { y: 20, label: "Nước táo" },
        { y: 14, label: "Nước táo" }
      ]
    }]
  }
    return(
      <main>
         <CanvasJSChart options = {options} 
				containerProps={ { width: 'auto', height: '300px' }}
                
			/>

      </main>
    )
}