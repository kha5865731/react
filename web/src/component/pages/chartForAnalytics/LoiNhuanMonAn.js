import React from 'react';
import CanvasJSReact from '@canvasjs/react-charts';
//var CanvasJSReact = require('@canvasjs/react-charts');
 
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default function App (){	
  const options = {
    animationEnabled: true,
    title:{
      text: "Lợi nhuận mua món ăn"
    },
    theme: "dark1",
    axisX: {
      valueFormatString: "MMM"
    },
    axisY: {
      title: "Sales (in USD)",
      prefix: "$"
    },
    data: [{
      yValueFormatString: "$#,###",
      xValueFormatString: "MMMM",
      type: "spline",
      dataPoints: [
        { y: 20, label: "Phở gà" },
        { y: 32, label: "Hambeger" },
        { y: 20, label: "Burito" },
        { y: 14, label: "Cơm gà" },
        { y: 12, label: "Khoai tây chiên" },
        { y: 10, label: "Bánh mì" },
        { y: 20, label: "Sushi" },
        { y: 11, label: "Bánh sườn bò" },
        { y: 20, label: "Mì cay" },
        { y: 14, label: "Bánh cuốn" }
        // { x: new Date(2017, 0), y: 25060 },
        // { x: new Date(2017, 1), y: 27980 },
        // { x: new Date(2017, 2), y: 42800 },
        // { x: new Date(2017, 3), y: 32400 },
        // { x: new Date(2017, 4), y: 35260 },
        // { x: new Date(2017, 5), y: 33900 },
        // { x: new Date(2017, 6), y: 40000 },
        // { x: new Date(2017, 7), y: 52500 },
        // { x: new Date(2017, 8), y: 32300 },
        // { x: new Date(2017, 9), y: 42000 },
        // { x: new Date(2017, 10), y: 37160 },
        // { x: new Date(2017, 11), y: 38400 },
      ]
    }]
  }
    return(
      <main>
         <CanvasJSChart options = {options} 
				containerProps={{ width: 'auto', height: '250px' }}
                
			/>

      </main>
    )
}