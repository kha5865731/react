import React from 'react';
import "./Analytics.css";
import TongDoanhThu from "./chartForAnalytics/TongDoanhThu";
import TyLeMuaMonAn from "./chartForAnalytics/TyLeMuaMonAn";
import LoiNhuanMonAn from "./chartForAnalytics/LoiNhuanMonAn";
import DoanhThuVSLoiNhuan from "./chartForAnalytics/DoanhThuVSLoiNhuan";
import TyLeMuaNuoc from "./chartForAnalytics/TyLeMuaNuoc";
import LoiNhuanNuoc from "./chartForAnalytics/LoiNhuanNuoc";

export default function App (){	
    return(
      <main>
        <div className='chart'>
          <div className='TongDoanhThu'> <TongDoanhThu /> </div>

          <div className='TyLeMuaMonAn' > <TyLeMuaMonAn /> </div>

          <div className='DoanhThu'>
            <div>
               <h2>Tổng Doanh thu</h2>
              <div> 200.000VND </div>
            </div>
           
            <div className='LoiNhuan'>
              <h2>Tổng Lợi nhuận</h2>
              <div>
                50.000VND
              </div>
            </div>
          </div>
          
            <div className='DoanhThuVSLoiNhuan'>
              <DoanhThuVSLoiNhuan />
            </div>

            <div className='LoiNhuanMonAn'>
              <LoiNhuanMonAn />
            </div>

             <div className='LoiNhuanNuoc'>
              <LoiNhuanNuoc />
            </div>
            
            <div className='TyLeMuaNuoc'>
              <TyLeMuaNuoc />
              
            </div>

           
        </div>   
      </main>
    )
}