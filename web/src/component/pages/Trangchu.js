import React,{useState} from 'react';
import "./Trangchu.css";

export default function TrangChu() {
  const [count, setCount] = useState(0);

	const increase = () => setCount(count + 1);
	const decrease = () => setCount(count - 1);

  return (
    <main>
      <div className='menu'>Món chính</div>
      <div className='food_gird'>
        <div className='block'>
          <p> Phở gà</p>
          <img className='img-pho-ga'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Hambeger</p>
          <img className='hambeger'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Burito</p>
          <img className='burito'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Cơm gà</p>
          <img className='com-ga'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Khoai tây chiên</p>
          <img className='KhoaiTayChien'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Bánh mì</p>
          <img className='banh-mi'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Sushi</p>
          <img className='sushi'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Bánh sườn bò </p>
          <img className='BanhSuonBo'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Mì cay </p>
          <img className='micay'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Bánh cuốn </p>
          <img className='banhcuon'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

      </div>
      <div className='menu drink'>
        Món thêm
      </div>

      <div className='food_gird'>

        <div className='block'>
          <p> Coca cola</p>
          <img className='Coca-cola'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Pesi</p>
          <img className='Pesi'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Chai Nước</p>
          <img className='NuocLoc'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Nước cam</p>
          <img className='Nuoccam'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Trà bí đao</p>
          <img className='Trabidao'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Trà sữa</p>
          <img className='Trasua'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button  className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Nước mía</p>
          <img className='Nuocmia'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Nước dừa </p>
          <img className='Nuocdua'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Nước táo </p>
          <img className='Nuoctao'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button className='button_buy' type='button'>BUY</button>
        </div>

        <div className='block'>
          <p> Monster </p>
          <img className='Monster'></img>
          <div className='button_add'>
            <div>{count}</div>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
          </div>
          <button className='button_buy' type='button'>BUY</button>
        </div>
        
      </div>
    </main>  
  )
}
