import React from 'react';
import "./Page404.css";

export default function Page404() {
  return (
    <main className='page'>
      Page not Found 404
    </main>
  );
}
