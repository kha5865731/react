import React from 'react'
import ComGa from "./../../image/menu/com-ga.jpg";
import CoCa from "./../../image/menu/cocacola.jpg";
import BanhSuonBo from "./../../image/menu/banhsuonbo.jpg";
import Nuoc from "./../../image/menu/nuocloc.jpg";
import KhoaiTay from "./../../image/menu/KhaiTayChien.jpg";
import Micay from "./../../image/menu/micay.jpg";
import "./Payment.css";

export default function ProductList() {
  return (
    <main>
        <div className='fontpayment'>
          <div className='table_name_payment'>
            <div> Số lượng</div>
            <div> Tên sản phẩm </div>
            <div> Hình ảnh </div>
            <div> Giá sản phẩm </div>
            <div> Thành tiền </div>
          </div>

          <div className='product'>
              <div className='SoLuong'>x1</div>
              <div className='TenSanPham'> Cơm gà </div>
              <img src={ComGa}></img>
              <div className='GiaSanPham'> 80.000VND</div>
              <div className='ThanhTien'> 80.000VND</div>
          </div>

          <div className='product'>
              <div className='SoLuong'>x2</div>
              <div className='TenSanPham'> Coca cola </div>
              <img src={CoCa}></img>
              <div className='GiaSanPham'> 10.000VND</div>
              <div className='ThanhTien'> 20.000VND</div>
          </div>

          <div className='product'>
              <div className='SoLuong'>x1</div>
              <div className='TenSanPham'> Bánh sườn bò </div>
              <img src={BanhSuonBo}></img>
              <div className='GiaSanPham'> 80.000VND</div>
              <div className='ThanhTien'> 80.000VND</div>
          </div>

          <div className='product'>
              <div className='SoLuong'>x4</div>
              <div className='TenSanPham'> Nước lọc </div>
              <img src={Nuoc}></img>
              <div className='GiaSanPham'> 5.000VND</div>
              <div className='ThanhTien'> 20.000VND</div>
          </div>

          <div className='product'>
              <div className='SoLuong'>x2</div>
              <div className='TenSanPham'> Khoai tây </div>
              <img src={KhoaiTay}></img>
              <div className='GiaSanPham'> 30.000VND</div>
              <div className='ThanhTien'> 60.000VND</div>
          </div>

          <div className='product'>
              <div className='SoLuong'>x2</div>
              <div className='TenSanPham'> Mì cay </div>
              <img src={Micay}></img>
              <div className='GiaSanPham'> 100.000VND</div>
              <div className='ThanhTien'> 200.000VND</div>
          </div>
          
        </div>

        <div className='pay'>
          <button type="button" class="button_mua focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Mua </button>
          <button type="button" class="button_huy focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Hủy</button>
          <div></div>
          <div className='TongTien'>Tổng tiền: 500.000VND</div>

        </div>
    </main>
  )
}
