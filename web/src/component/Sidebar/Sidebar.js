import React, { useState } from 'react';
import Person from "./../../image/person.jpg";
import Logo_shop from "./../../image/logo.png";
import { ReactComponent as Sun } from "./../../image/Sun.svg";
import { ReactComponent as Moon } from "./../../image/Moon.svg";
import {Link } from "react-router-dom";
import "./Sidebar.css";
import {
    FaTh,
    FaBars,
    FaUserAlt,
    FaRegChartBar,
    FaShoppingBag,
} from "react-icons/fa";
import {FaHeadSideVirus} from "react-icons/fa";
import {RiLoginBoxLine} from "react-icons/ri";
import {IoMdLogIn} from "react-icons/io";
import {BsSunFill} from "react-icons/bs";
import { Outlet, NavLink } from 'react-router-dom';

const Sidebar = () => {
    const [isOpen, setIsOpen] = useState(false);

    const [showGG, setShowMenuTab] = useState(false);

    const handleMouseEnterMenu = () => {
        setShowMenuTab(true);
    }
  
    const handleMouseLeaveMenu = () => {
        setShowMenuTab(false);
    }

    const toggle =()=> setIsOpen(!isOpen);

    const setDarkMode = () =>{
        document.querySelector("body").setAttribute('data-theme','dark')
    } 
    const setLightMode = () =>{
        document.querySelector("body").setAttribute('data-theme','light')
    }
    const toggleTheme = (e) =>{
       if(e.target.checked) setDarkMode();
       else setLightMode(); 
    }
    const menuItem = [
        {
            path: "TrangChu",
            name: "Trang chủ",
            icon: <FaTh />
        },
        
        {
            path: "analytics",
            name: "Biểu đồ",
            icon: <FaRegChartBar />
        },
        {
            path: "Payment",
            name: "Thanh toán",
            icon: <FaShoppingBag />
        },
        {
            path: "TaiKhoan",
            name: "Tài khoản",
            icon: <FaUserAlt />
        },
        {
            path: "AL",
            name: "Trí tuệ nhân tạo",
            icon: <FaHeadSideVirus />
        },
        {
            path: "/login",
            name: "Đăng xuất",
            icon: <RiLoginBoxLine />
        }
    ]
    return (
        <div >
            <div style={{ width: isOpen ? "210px" : "50px", position: "fixed", overflowY: "auto"}} className="sidebar">
                <div className="top_section">
                    <h1 style={{ display: isOpen ? "block" : "none" }} className="logo">Shopbuy</h1>
                    <div style={{ marginLeft: isOpen ? "50px" : "0px" }} className="bars">
                        <FaBars onClick={toggle} />
                    </div>
                </div>
                {
                    menuItem.map((item, index) => (
                        <NavLink to={item.path} key={index} className="link" activeClassName="active">
                            <div className="icon">{item.icon}</div>
                            <div style={{ display: isOpen ? "block" : "none" }} className="link_text">{item.name}</div>
                        </NavLink>
                    ))
                }
            </div>
            <div style={{ marginLeft: isOpen ? "210px" : "50px" }}>
                 <div className='Heder_Sidebar'>
                    <img src={Logo_shop} className='logo_Sidebar'></img>
                    <div>
                        <form class="max-w-sm px-4">
                            <div class="relative">
                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute top-0 bottom-0 w-6 h-6 my-auto text-gray-400 left-3" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                                </svg>
                                <input type="text" placeholder="Tìm kiếm" class="w-full py-3 pl-12 pr-4 text-gray-500 border rounded-md outline-none bg-gray-50 focus:bg-white focus:border-indigo-600" />
                            </div>
                        </form>
                    </div>
                    <div className='dark_mode'>
                        <input
                            className='dark_mode_input'
                            type='checkbox'
                            id='darkmode-toggle'
                            onChange={toggleTheme}
                        />
                        <label className='dark_mode_label' for='darkmode-toggle'>
                            <Sun />
                            <Moon />
                        </label>
                    </div>

                    <img className='bell_img_Sidebar' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAABVElEQVR4nO2VO0oDURSGR8F0ESGF4h608gGuwGCvixCXYCyC6A4EHzvRJjFaioUKxsrE3kQt4ycHzuDl5DqZZ7DIBxfCf5j/Y+7MnQTBhP8MsAV0gQ5QHae4wy+v4xAuAHWGkWy+KOkO0ONvZLZdhHTAaAaJ5cA00JJl8sURd2p5l0diOlpAUxw+cVUvfDb5Icmpm4625ps+8bkO901+n0J8Zzpqmp/5xE86XDH5Vwrxp+lY0/zBJ/7QYdnkqTAdZY37PnFfh7MFiOc07vnEjzpcdbKptGK51ulZj9rq8OWqOVklg7ji9Bxodhp1nNpOtpxBvOT0vEQdp/AD0nCyvQziXafnWtfwB8QHcJVBfBlLYtGz9002NoIkADPALdm5ka4k4hPy4yKuVF6wvGnGEcvfV940Ym/3hBDgzbOV3aBogGOP+Ggc4pLK5c5lye9S4eK8+AHmhsINZO+xIAAAAABJRU5ErkJggg=="></img>
                    <Link to ="/Home/Payment">
                        <img className='shop-cart_Sidebar' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA+klEQVR4nO2VOwoCMRBAt9ReRO0svIBewcJFsFc8go2FKIIH8BSCl9Daz2LlHfzUdor/JwvxB4K7EzYg66sn85KZTGJZoQSY8WQBlE2JHd45Axkj8jtAX8k7lkmAkhI7psVRYAdcgLhp+UCdumpaXCMYJt/E6YDES2klUkATOAjFPWETHhtoCcWVT8mmHhaOX07ul+vHKXGTelg8UrFxgXiuVWYlbgjEXd3Hxb1cJ4E4r9PbJDK2QESnt1LxUFxmzTGqS4UJoA0cBdINEJOKs265gb0P4UG1MSeShhegCKyBFWDrxnlGJfr6vXmN+wmxrZIugYJu3B/LFDfh0re7WS9uBgAAAABJRU5ErkJggg=="/>
                    </Link>
                    
                    <div >
                        <img onMouseEnter={handleMouseEnterMenu} className='avatar_Sidebar' src={Person} alt='user'/>
                        {showGG &&
                            <ul className='box_tap' onMouseLeave={handleMouseLeaveMenu}>
                                <Link to="/Home/TaiKhoan">
                                    <button type='button'>
                                        <FaUserAlt className='icon_tap'></FaUserAlt>
                                        <li>Tài khoản</li> 
                                    </button>
                                </Link>
                                
                                <Link>
                                    <button type='button'>
                                        <BsSunFill className='icon_tap'></BsSunFill>
                                        <li style={{textAlign:"left",marginLeft:"10px"}}>Chế độ</li>
                                    </button>
                                </Link>
                                
                                <Link to="/login">
                                    <button type='button'>
                                        <IoMdLogIn className='icon_tap'></IoMdLogIn>
                                        <li>Đăng xuất</li>
                                    </button>
                                </Link>
                                
                            </ul>
                        }
                    </div>

                </div>

                <Outlet />
            </div>
        </div>
    );
};

export default Sidebar;
// https://www.youtube.com/watch?v=q9KhOfg_uLI