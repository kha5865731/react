import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Login from "./component/Login/Login";
import Sidebar from './component/Sidebar/Sidebar';
import TrangChu from './component/pages/Trangchu';
import TaiKhoan from './component/pages/TaiKhoan';
import Analytics from './component/pages/Analytics';
import Comment from './component/pages/Comment';
import Payment from './component/pages/Payment';
import Register from "./component/Login/register";
import Page404 from "./component/pages/Page404";
import AL from "./component/pages/AL";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path='/Login' element={<Login />} />
        <Route path='/Home' element={<Sidebar />}>
          <Route path="TrangChu" element={<TrangChu />} />
          <Route path="TaiKhoan" element={<TaiKhoan />} />
          <Route path="comment" element={<Comment />} />
          <Route path="analytics" element={<Analytics />} />
          <Route path="Payment" element={<Payment />} />
          <Route path="AL" element={<AL />} />
        </Route>
        <Route path='/DangKy' element={<Register />} />
        <Route path='*' element={<Page404 />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
